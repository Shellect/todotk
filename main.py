from tkinter import *
from tkinter import messagebox
import random

HEIGHT = 600
WIDTH = 600

tasks = []


def update_listbox():
    listbox.delete(0, END)
    for task in tasks:
        listbox.insert(END, task)


def add_task():
    task = text_input.get()
    if task != '':
        # Добавить элемент в список
        tasks.append(task)
        update_listbox()
    else:
        messagebox.showwarning('Warning', 'Enter the task')
    text_input.delete(0, END)


def del_one():
    task = listbox.get('active')
    if task in tasks:
        # Удалить элемент из списка
        tasks.remove(task)
    update_listbox()


def del_all():
    confirmed = messagebox.askyesno(
        'Confirm',
        'Do you really want to delete all?'
    )
    if confirmed:
        global tasks
        tasks = []
        update_listbox()


def sort_asc():
    tasks.sort()
    update_listbox()


def sort_desc():
    tasks.sort()
    tasks.reverse()
    update_listbox()


def choose_random():
    if len(tasks) > 0:
        task = random.choice(tasks)
        label_display['text'] = task
    else:
        messagebox.showwarning('Warning', 'No tasks')


def show_number_of_tasks():
    n = len(tasks)
    message = 'У Вас %s задач' % n
    label_display['text'] = message


if __name__ == '__main__':
    root = Tk()
    root.title('To Do List')
    root.geometry('%dx%d' % (WIDTH, HEIGHT))
    root.resizable(False, False)

    img = PhotoImage(file='img/todo_bg.gif')
    background = Label(root, image=img)
    background.place(relwidth=1, relheight=1)

    root.option_add('*Font', '{Comic Sans MS} 10')
    root.option_add('*Background', 'green')
    root.option_add('*Foreground', 'white')

    frame = Frame(root, bd=10)
    frame.place(
        relx=.1, rely=.1,
        relwidth=.8, relheight=.8
    )

    label_title = Label(
        frame,
        text='Список моих дел',
        fg='dark blue',
        font='{Comic Sans MS} 16'
    )
    label_title.place(relx=.3, relwidth=.7)

    label_display = Label(frame, text='')
    label_display.place(relx=.4, rely=.1)

    text_input = Entry(frame, width=15)
    text_input.place(relx=.3, rely=.15, relwidth=.6)

    # Add_task task
    button_add_task = Button(frame, text='Добавить\nзадачу', command=add_task)
    button_add_task.place(relwidth=.25)

    # Del_one task
    button_del_one = Button(frame, text='Удалить\nзадачу', command=del_one)
    button_del_one.place(rely=.15, relwidth=.25)

    # Del_all task
    button_del_all = Button(frame, text='Удалить\nвсё', command=del_all)
    button_del_all.place(rely=.30, relwidth=.25)

    # Sort_asc task
    button_sort_asc = Button(frame, text='Сортировать\n(А-Я)', command=sort_asc)
    button_sort_asc.place(rely=.45, relwidth=.25)

    # Sort_desc task
    button_sort_desc = Button(frame, text='Сортировать\n(Я-А)', command=sort_desc)
    button_sort_desc.place(rely=.60, relwidth=.25)

    # Choose_random task
    button_choose_random = Button(frame, text='Выбрать\nслучайную', command=choose_random)
    button_choose_random.place(rely=.75, relwidth=.25)

    # Show_number_of_tasks task
    button_show_number_of_tasks = Button(frame, text='Показать\nколичество', command=show_number_of_tasks)
    button_show_number_of_tasks.place(rely=.90, relwidth=.25)

    listbox = Listbox(frame)
    listbox.place(relx=.3, rely=.25, relwidth=.6, relheight=.7)

    root.mainloop()
